package com.company;
import java.io.*;
import java.util.*;

public class Main {

    //TODO подумать как сразу читать в целочисленный массив...
    public static void main(String[] args) throws IOException {
        String[] strarray = null;
        BufferedReader reader = new BufferedReader(new FileReader("numbers.txt")); //чтение из файла
        String numstr;
        numstr = reader.readLine();//запись в строковую переменную
        strarray = numstr.split(","); //разбиение строки и запись в массив строковых данных
        int len = strarray.length;
        Integer[] intarray = new Integer[len]; // Создание массива целочисленного типа,
        // ниже заполнение из строковога массива
        for (int i = 0; i <= len-1; i++){
            intarray[i] = Integer.parseInt(strarray[i]);
            }

        Arrays.sort(intarray);
        System.out.println(Arrays.toString(intarray)); // Вывод по возрастанию
        Arrays.sort(intarray, Collections.reverseOrder());
        System.out.println(Arrays.toString(intarray)); // Вывод по убыванию
        }

}